# Typist

A minimal typing test application for the command line.



## Get Started

This application requires a working ruby environment.

- Clone the repository.
- Run `bundle install`.
- Run `exe/typist`. The application doesn't take any arguments.
