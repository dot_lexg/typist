# frozen_string_literal: true

class CompletedFlight
  def self.from_h(started:, prompt:, keystrokes:, timing:)
    new(started, prompt, keystrokes, timing)
  end

  def initialize(started, prompt, keystrokes, timing)
    @started = started
    @prompt = prompt
    @keystrokes = keystrokes
    @timing = timing
    freeze
  end
  attr_reader :started, :prompt, :keystrokes, :timing

  def prompt_str = @prompt.join(' ')
  def length = prompt_str.length
  def total_time = timing.sum
  def wpm = length.fdiv(total_time) * 12000

  def accuracy
    mistakes = []
    position = 0
    keystrokes.each_char do |c|
      case c
      when "\b"
        position -= 1
      when prompt_str[position]
        position += 1
      else
        mistakes[position] = true
        position += 1
      end
    end

    mistakes = mistakes.count(&:itself)
    (position - mistakes).fdiv(position)
  end

  def to_colored_str
    <<~INFO

        #{prompt_str.light_black}

      #{summary}
    INFO
  end

  def summary
    <<~INFO
      total time: #{total_time.fdiv(1000).round(2)} seconds
      avg speed:  #{wpm.round(1)} wpm
      accuracy:   #{(accuracy * 100).round(1)}%
    INFO
  end

  def to_h
    {
      started: @started,
      prompt: @prompt,
      keystrokes: @keystrokes,
      timing: @timing,
    }.freeze
  end
end
