# frozen_string_literal: true

require 'io/console'

class Terminal
  def initialize
    raise 'This program must be run interactively' unless $stdin.tty?
  end

  def raw(...) = $stdin.raw(...)
  def get = $stdin.read(1)

  def print(str) = $stdout.print(String(str).gsub("\n", "\r\n"))
  def puts(str = '') = $stdout.print("#{String(str).chomp.gsub("\n", "\r\n")}\r\n")
  def bell = $stdout.print("\a")
  def clear = $stdout.print("\e[H\e[J")
  def goto(x, y) = $stdout.print("\e[#{y + 1};#{x + 1}H")
end
