# frozen_string_literal: true

require 'colorize'
require_relative 'completed_flight'

class Flight
  def initialize(prompt)
    @prompt = prompt
    @started = Time.now
    @timing = []
    @keystrokes = +''
    @progress = +''
    @last_keystroke_time = nil
  end

  def prompt_str = @prompt.join(' ')

  def to_colored_str
    result = +"\n  "
    @progress.chars.zip(prompt_str.chars).each do |actual, expected|
      result << (actual == expected ? actual.light_black : actual.black.on_red)
    end
    result << "\e7" # DEC Save Cursor
    result << prompt_str[@progress.length..]
    result << "\e8" # DEC Restore Cursor

    result.freeze
  end

  def <<(char)
    @keystrokes << char
    @progress << char
    @timing << delta_time
  end

  def backspace
    return if @progress.empty?

    @keystrokes << "\b"
    @progress.chop!
    @timing << delta_time
  end

  def done?
    @progress == prompt_str
  end

  def complete
    CompletedFlight.new(@started, @prompt, @keystrokes, @timing)
  end

  private

  def delta_time
    if @last_keystroke_time
      # written this way so as to carry error from rounding to next frame
      delta_millis = ((Time.now - @last_keystroke_time) * 1000).round
      @last_keystroke_time += delta_millis.fdiv(1000)
      delta_millis
    else
      @last_keystroke_time = Time.now
      0
    end
  end
end
