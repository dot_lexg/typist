# frozen_string_literal: true

require 'pathname'

class WordGenerator
  def initialize(path: Pathname('wordlist.txt').expand_path(__dir__), limit: 200)
    @full_list = Pathname(path).read.lines(chomp: true)
    @limit = limit
  end

  def generate(word_count)
    list.sample(word_count)
  end

  def list
    @full_list[0...@limit]
  end
end
